///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 09a - Cat Empire!
///
/// @file cat.cpp
/// @version 1.0
///
/// Exports data about all cats
///
/// @author @todo yourName <@todo yourMail@hawaii.edu>
/// @brief  Lab 09a - Cat Empire! - EE 205 - Spr 2021
/// @date   @todo dd_mmm_yyyy
///////////////////////////////////////////////////////////////////////////////

#include <cassert>
#include <random>
#include <queue>
#include <string>

#define CAT_NAMES_FILE "names.txt"

#include "cat.hpp"

using namespace std;

Cat::Cat( const std::string newName ) {
	setName( newName );
}


void Cat::setName( const std::string newName ) {
	assert( newName.length() > 0 );

	name = newName;
}


std::vector<std::string> Cat::names;


void Cat::initNames() {
	names.clear();
	cout << "Loading... ";

	ifstream file( CAT_NAMES_FILE );
	string line;
	while (getline(file, line)) names.push_back(line);

	cout << to_string( names.size() ) << " names." << endl;
}


Cat* Cat::makeCat() {
	// Get a random cat from names
	auto nameIndex = rand() % names.size();
	auto name = names[nameIndex];

	// Remove the cat (by index) from names
	erase( names, names[nameIndex] );

	return new Cat( name );
}


bool CatEmpire::empty() {
   if( topCat== nullptr)
      return true;
   else
      return false;
}

void CatEmpire::addCat( Cat* newCat ) {
   if ( empty() ) {
      topCat = newCat;
      return;
   }

   addCat( topCat, newCat );
}


void CatEmpire::addCat( Cat* atCat, Cat* newCat ) {

   assert( newCat != nullptr);

      if( atCat->name > newCat->name ) { // if Newcat is less than current Node
         if( atCat->left == nullptr) {
            atCat->left = newCat;
         } else {
            addCat( atCat->left, newCat );
         }
      }// End Case of newCat being less than current Tree Node

         else {
            if ( atCat->right == nullptr) {
               atCat->right = newCat;
            } else {
                  addCat( atCat->right, newCat ) ;
               }
            }
}
         


void CatEmpire::catFamilyTree() const {
	if( topCat == nullptr ) {
		cout << "No cats!" << endl;
		return;
	}

	dfsInorderReverse( topCat, 1 );
}



void CatEmpire::catList() const {
	if( topCat == nullptr ) {
		cout << "No cats!" << endl;
		return;
	}

	dfsInorder( topCat );
}





void CatEmpire::catBegat() const {
	if( topCat == nullptr ) {
		cout << "No cats!" << endl;
		return;
	}

	dfsPreorder( topCat );
}

void CatEmpire::dfsInorderReverse( Cat* atCat, int depth ) const {
   
   if ( topCat == nullptr)
      return;
   if ( atCat == nullptr ) {
      depth--;
      return;
   }  
   
   depth++;
   dfsInorderReverse( atCat->right, depth);
      cout << string( 6 * (depth-2), ' ' ) << atCat->name;
   if( atCat->left != nullptr &&  atCat->right != nullptr)
      cout << "<";
   if( atCat->left != nullptr &&  atCat->right == nullptr)
      cout <<  char(92) ;
   if( atCat->left == nullptr &&  atCat->right != nullptr)
      cout << "/";
   cout << endl;
   dfsInorderReverse( atCat->left, depth);

}



void CatEmpire::dfsInorder( Cat* atCat ) const {
   
   if ( topCat == nullptr)
      return;
   if( atCat != nullptr)  {

   dfsInorder( atCat->left);
   cout << atCat->name << endl;
   dfsInorder( atCat->right);
   }


}
void CatEmpire::dfsPreorder( Cat* atCat ) const {
   
   if( atCat == nullptr)
      return;

   if( atCat->left != nullptr || atCat->right != nullptr){
      cout << atCat->name;
      cout << " begat ";
      if(atCat->left != nullptr)
         cout<< atCat->left->name;
      if(atCat->left != nullptr && atCat->right != nullptr)
         cout<< " and ";
      if(atCat->right != nullptr)
         cout << atCat->right->name;
      cout<< endl;
   }
   dfsPreorder ( atCat->left);
   dfsPreorder ( atCat->right);
}


void CatEmpire::catGenerations() {
      queue<Cat*> catQueue;
      catQueue.push(topCat);
      
      //Initialize Elements in the Previous Generation 
      int generation = 1;
      int prevGenCount = 1;
      int count;
          
         //Iterate Down the Generations
            while( !catQueue.empty() ) {

            getEnglishSuffix( generation );
            cout << " Generation" << endl;
            count=0;

            //Loops For how many Nodes(Parents) there were in the previous Generation
            for(int j = prevGenCount; j > 0; j--){

            Cat* cat = catQueue.front(); 
            cout<< " " << cat->name << " " ;
            catQueue.pop();

            if( cat == nullptr) 
               return;
            if(cat->left != nullptr){
               catQueue.push ( cat->left);
               count++;
            }
            if(cat->right != nullptr){
               catQueue.push (cat->right);
               count++;
            }
            }
        prevGenCount=count;
        cout<< endl;
        generation++;
         }
      
}

void CatEmpire::getEnglishSuffix ( int n ) {
   
   string ordinals[] = { "st", "nd", "rd", "th"};
   int i;
   n %= 100; //Normalize Value of n
   
   //If its between 3 and 21 its automatically th so i = 3 to return odinal[3]
   if ( 3 < n && n <21)
   i=3;
   
   //Special cases
   else{
   switch( n % 10) {
      case (1) : i=0;
                 break;
      case (2) : i=1;
                 break;
      case (3) : i=2;
                 break;
      default: i=3;
               break;
   };
   }

   cout << n << ordinals[i];
}
